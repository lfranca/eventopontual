class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :name
      t.string :email
      t.integer :document_number
      t.integer :document_type
      t.string :phone_area_code
      t.string :phone_number
      t.string :mobile_phone_area_code
      t.string :mobile_phone_number

      t.timestamps null: false
    end
  end
end
