json.extract! @customer, :id, :name, :email, :document_number, :document_type, :phone_area_code, :phone_number, :mobile_phone_area_code, :mobile_phone_number, :created_at, :updated_at
