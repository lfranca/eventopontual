module ApplicationHelper
    def br_date_format date
      if date
        date.strftime("%d/%m/%Y")
      else
         " "
      end
  end
end
