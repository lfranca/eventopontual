class Customer < ActiveRecord::Base

  validates_format_of :email, :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i, :on => :create
  validates :name,
    presence: true
  
  validates :email,
    presence: true,
    uniqueness: { case_sensitive: false },
    length: { maximum: 100 }

  validates :document_number,
    presence: true,
    numericality: true,
    uniqueness: true

  validates :document_type,
    presence: true
    
end
