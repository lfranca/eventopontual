class Product < ActiveRecord::Base
  
  validates :code,
    presence: true,
    uniqueness: { case_sensitive: false }
  
  validates :name,
    presence: true,
    uniqueness: { case_sensitive: false },
    length: { maximum: 100 }

  validates :price,
    presence: true,
    numericality: true

end
