class Event < ActiveRecord::Base

    validates :name,
    presence: true,
    uniqueness: { case_sensitive: false },
    length: { maximum: 100 }


    validates_presence_of :start_date, :end_date

    validate :end_date_is_after_start_date

  def end_date_is_after_start_date
    return if end_date.blank? || start_date.blank?
    if end_date < start_date
      errors.add :base, I18n.t('end_date_greater_then_start_date')
    end 
  end    

end
